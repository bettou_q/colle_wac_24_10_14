@extends('layouts.default')
@section('content')
<h2>S'enregistrer</h2>

{{Form::open(array('url' => 'users', 'method' => 'POST'))}}
	
	<p>{{Form::text('username', null, array('placeholder' => 'Pseudonyme'))}}</p>
	<p>{{Form::text('name', null, array('placeholder' => 'Nom'))}}</p>
	<p>{{Form::text('firstname', null, array('placeholder' => 'Prénom'))}}</p>
	<p>{{Form::email('email', null, array('placeholder' => 'Adresse electronique'))}}</p>
	<p>{{Form::password('password', array('placeholder' => 'Mot de passe'))}}</p>
	<p>{{Form::submit('s\'enregirstrer')}}</p>
{{Form::close()}}
@stop