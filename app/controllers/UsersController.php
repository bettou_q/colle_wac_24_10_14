<?php

class UsersController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('users.index');
	}

	public function getLogin()
	{
		return View::make('users.login');
	}

	public function postLogin()
	{

		$username = Input::get('username'); 
		$password = Input::get('password');
		if(Auth::attempt(array('username' => $username, 'password' => $password), true))
		{
			return Redirect::to('videos/index')
				->with('message', 'Successfuly connected');
		}else{
			return Redirect::to('users/add')
					->with('error', 'Connection failed');
		}
	}
 

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$validator = Validator::make(Input::all(), User::$rules);
		if($validator->passes())
		{
			$user = New User;
			$user->username = Input::get('username');
			$user->name = Input::get('name');
			$user->firstname = Input::get('firstname');
			$user->email = Input::get('email');
			$user->password = Hash::make(Input::get('password'));
			$user->save();
			return Redirect::to('users/login')
				->with('message', 'félicitation, vous êtes inscris !');
		}
		else
		{
			return Redirect::to('users/add')
				->withError($validator)
					->with('error', 'Inscription non valide');
		}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
