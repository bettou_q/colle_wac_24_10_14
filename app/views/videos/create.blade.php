@extends('layouts.default')
@section('content')
<h2>Ajouter une vidéo</h2>

{{Form::open(array('url' => 'videos', 'method' => 'POST'))}}
	
	<p>{{Form::text('title', null, array('placeholder' => 'Titre'))}}</p>
	<p>{{Form::text('content', null, array('placeholder' => 'Content'))}}</p>
	<p>{{Form::text('tag', null, array('placeholder' => 'Tag'))}}</p>
	<p>{{Form::file('file')}}</p>
	<p>{{Form::submit('Ajouter')}}</p>

{{Form::close()}}
@stop