@extends('layouts.default')
@section('content')
<h2>Se connecter</h2>

{{Form::open(array('url' => 'users/login', 'method' => 'POST'))}}
	
	<p>{{Form::text('username', null, array('placeholder' => 'Pseudonyme'))}}</p>
	<p>{{Form::password('password', array('placeholder' => 'Mot de passe'))}}</p>
	<p>{{Form::submit('Se connecter')}}</p>

{{Form::close()}}

@stop