<?php

class VideosController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('videos.index');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('videos.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(Input::all(), Videos::$rules);
		//$destination_path = public_path('uploads/'. Auth::user()->id . Input::getClientOriginalName());
		if($validator->passes())
		{
			$video = New Video;
			$video->title = Input::get('username');
			$video->content = Input::get('content');
			$video->tag = Input::get('tag');
			$video->username = Auth::user()->username;
			$video->filename = Input::file('file')->getClientOriginalName();
			$video->size = Input::file('file')->getSize();
			$video->mime = Input::file('file')->getMimeType();

			$video->save();
			return Redirect::to('users/login')
				->with('message', 'félicitation, vous êtes inscris !');
		}
		else
		{
			return Redirect::to('users/add')
				->withError($validator)
					->with('error', 'Inscription non valide');
		}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
